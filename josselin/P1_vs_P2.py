# -*- coding: utf-8 -*-
"""
Created on Wed Dec 29 16:23:28 2021

@author: Manon
"""

import numpy as np
import matplotlib.pyplot as plt

plt.close("all")

############################
######## Simulation ########
############################

# P1 parameters
P1_0 = 10000
b1 = 0.1
d1 = 0.01

# P2 parameters
P2_0 = 100
b2 = 0.11
d2 = 0.01

# Consumption
c = 1

# Awakness
t_a = 60
delta_t_a = 1
alpha = 1
epsilon = 0.001

b2_a = d2 - (b1-d1+d2)*alpha+b1-d1+epsilon

t_a_t = np.linspace(0,delta_t_a,delta_t_a+1)
b2_t = d2 + (b2_a-b2)/delta_t_a*t_a_t+b2-d2

# Time parameters
n = 200
t = np.linspace(0,n,n+1)

######## Without awakness ########
##################################

# P2 computation
P2 = P2_0 * np.exp((b2-d2)*t)

# P1 computation
P1 = P1_0 * np.exp((b1-d1)*t) + c*P2_0/((b2-d2)-(b1-d1))*(np.exp((b1-d1)*t)-np.exp((b2-d2)*t))

# Regularization
argwhere = np.argwhere(P1<0)
if argwhere.shape[0] > 0:
  P2[argwhere]=0
  P1[argwhere]=0

######## With awakness ########
###############################

P1_a = np.zeros(t.shape)
P2_a = np.zeros(t.shape)
for i in range(t.shape[0]):
    
    if t[i] < t_a:
        P2_a[i] = P2_0 * np.exp((b2-d2)*t[i])
        P1_a[i] = P1_0 * np.exp((b1-d1)*t[i]) + c*P2_0/((b2-d2)-(b1-d1))*(np.exp((b1-d1)*t[i])-np.exp((b2-d2)*t[i]))
    elif (t[i] >= t_a) and (t[i] <= t_a + delta_t_a):
        P2_a[i] = P2_a[i-1] * np.exp((b2_t[int(t[i])-t_a]-d2)*1)
        P1_a[i] = P1_a[i-1] * np.exp((b1-d1)*1) + c*P2_a[i-1]/((b2_t[int(t[i])-t_a]-d2)-(b1-d1))*(np.exp((b1-d1)*1)-np.exp((b2_t[int(t[i])-t_a]-d2)*1))
    elif t[i] > t_a + delta_t_a:
        P2_a[i] = P2_a[t_a + delta_t_a] * np.exp((b2_t[-1]-d2)*((t[i]-(t_a + delta_t_a))))
        P1_a[i] = P1_a[t_a + delta_t_a] * np.exp((b1-d1)*(t[i]-(t_a + delta_t_a))) + c*P2_a[t_a + delta_t_a]/((b2_t[-1]-d2)-(b1-d1))*(np.exp((b1-d1)*(t[i]-(t_a + delta_t_a)))-np.exp((b2_t[-1]-d2)*(t[i]-(t_a + delta_t_a))))
        
# Regularization
argwhere_a = np.argwhere(P1_a<0)
if argwhere_a.shape[0] > 0:
  P2_a[argwhere_a]=0
  P1_a[argwhere_a]=0

#####################################
######## Display the results ########
#####################################

######## Without awakness ########
##################################

plt.figure()
if not argwhere.shape[0] > 0:
    plt.plot(t,P1,label="Lézards",color="green",linestyle = "None", marker="_")
    plt.plot(t,P2,label="Humains",color="red",linestyle = "None", marker="_")
else:
    plt.plot(t[0:argwhere[1,0]],P1[0:argwhere[1,0]],label="Lézards",color="green",linestyle = "None", marker="_")
    plt.plot(t[0:argwhere[1,0]],P2[0:argwhere[1,0]],label="Humains",color="red",linestyle = "None", marker="_")
plt.legend()
plt.xlabel("Temps")
plt.ylabel("Population")
plt.grid(True)
#plt.savefig("FiguresSystemique//???.png",dpi=300)

######## With awakness ########
###############################

plt.figure()
if not argwhere_a.shape[0] > 0:
    plt.plot(t,P1,label="Lézards",color="green",linestyle = "None", marker="_")
    plt.plot(t,P2,label="Humains",color="red",linestyle = "None", marker="_")
    plt.plot(t,P1_a,label="Lézards avec prise de conscience",color="green",linestyle = "None", marker="+")
    plt.plot(t,P2_a,label="Humains avec prise de conscience",color="red",linestyle = "None", marker="+")
else:
    plt.plot(t[0:argwhere[1,0]],P1[0:argwhere[1,0]],label="Lézards",color="green",linestyle = "None", marker="_")
    plt.plot(t[0:argwhere[1,0]],P2[0:argwhere[1,0]],label="Humains",color="red",linestyle = "None", marker="_")
    plt.plot(t[0:argwhere_a[1,0]],P1_a[0:argwhere_a[1,0]],label="Lézards avec prise de conscience",color="green",linestyle = "None", marker="+")
    plt.plot(t[0:argwhere_a[1,0]],P2_a[0:argwhere_a[1,0]],label="Humains avec prise de conscience",color="red",linestyle = "None", marker="+")
plt.legend()
plt.xlabel("Temps")
plt.ylabel("Population")
plt.grid(True)
#plt.savefig("FiguresSystemique//???.png",dpi=300)